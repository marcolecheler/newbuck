<?php
	//	Vorbelegte Werte auslesen
	if (isset($_SESSION['Kontaktdaten_Vorname']))
	{
		$inputVorname = $_SESSION['Kontaktdaten_Vorname'];
		$inputNachname = $_SESSION['Kontaktdaten_Nachname'];
		$inputEMail = $_SESSION['Kontaktdaten_EMail'];
		$inputKontoinhaber = $_SESSION['Kontaktdaten_Kontoinhaber'];
		$inputBank = $_SESSION['Kontaktdaten_Bank'];
		$inputIBAN = $_SESSION['Kontaktdaten_IBAN'];
		$inputBIC = $_SESSION['Kontaktdaten_BIC'];
		
	}
	else
	{
		$inputVorname = "TMarco";
		$inputNachname = "TLecheler";
		$inputEMail = "TEmail@marco.lecheler.com";
		$inputKontoinhaber = "TInhaber Lecheler";
		$inputBank = "TBank";
		$inputIBAN = "TIBAN";
		$inputBIC = "TBIC";
	}
	
?>

<h1>Kontaktdaten</h1>
<p>
	<form method="post" action="?site=AntragNeu&sdata=Kontaktdaten&nextSite=AntragUebersicht">
		<fieldset>
			<legend>Kontaktdaten</legend>
			<table>
				<tr>
					<th><label>Vorname:</label></th>
					<td><input name="Vorname" value="<?php echo $inputVorname ?>" required/></td>
				</tr>
				<tr>
					<th><label>Nachname:</label></th>
					<td><input name="Nachname" value="<?php echo $inputNachname ?>" required/></td>
				</tr>
				<tr>
					<th><label>E-Mail:</label></th>
					<td><input type="email" name="EMail" value="<?php echo $inputEMail ?>" required/></td>
				</tr>
			</table>
	
		</fieldset>
		
		<fieldset>
			<legend>Kontodaten</legend>
			<table>
				<tr>
					<th><label>Kontoinhaber:</label></th>
					<td><input name="Kontoinhaber" value="<?php echo $inputKontoinhaber ?>" required/></td>
				</tr>
				<tr>
					<th><label>Bank:</label></th>
					<td><input name="Bank" value="<?php echo $inputBank ?>" required/></td>
				</tr>
				<tr>
					<th><label>IBAN:</label></th>
					<td><input name="IBAN" value="<?php echo $inputIBAN ?>" required/></td>
				</tr>
				<tr>
					<th><label>BIC:</label></th>
					<td><input name="BIC" value="<?php echo $inputIBAN ?>" required/></td>
				</tr>
			</table>
	
		</fieldset>
		
		<br />
		<button type="submit">Weiter</button>
	</form>
</p>
