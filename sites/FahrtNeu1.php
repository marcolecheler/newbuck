<?php
	
	//	Vorbelegte Werte auslesen
	if(isset($_SESSION['Fahrt'.($_SESSION['Fahrten_Anzahl'] + 1).'Datum']))
	{
		echo "inputgefunden";
		$inputDatum = $_SESSION['Fahrt'.($_SESSION['Fahrten_Anzahl'] + 1).'Datum'];
		$inputMannschaft = $_SESSION['Fahrt'.($_SESSION['Fahrten_Anzahl'] + 1).'Mannschaft'];
		$inputOrt = $_SESSION['Fahrt'.($_SESSION['Fahrten_Anzahl'] + 1).'Ort'];
	}
	else
	{
		$inputDatum = date("Y-m-d");
		$inputMannschaft = "H1";
		$inputOrt = "";
	}
?>

<h1>Fahrt hinzuf&uuml;gen</h1>
<p>
	<form method="post">
		<fieldset>
			<legend>Allgemein</legend>
			<table>
				<tr>
					<th><label>Datum:</label></th>
					<td>
						<input type="date" name="Datum" value="<?php echo $inputDatum ?>" />
					</td>
				</tr>
				<tr>
					<th><label>Mannschaft:</label></th>
					<td>
						<select name="Mannschaft">
							<optgroup>Erwachsenen-Bereich</optgroup>
							<option value="H1" <?php  echo ($inputMannschaft == "H1") ? "selected":"" ?>>Herren I</option>
							<option value="H2" <?php  echo ($inputMannschaft == "H2") ? "selected":"" ?>>Herren II</option>
							<option value="D1" <?php  echo ($inputMannschaft == "D1") ? "selected":"" ?>>Damen</option>
							<optgroup>Jugend-Bereich</optgroup>
							<option value="mA" <?php  echo ($inputMannschaft == "mA") ? "selected":"" ?>>m&auml;nnliche A-Jugend</option>
							<option value="wA" <?php  echo ($inputMannschaft == "wA") ? "selected":"" ?>>weibliche A-Jugend</option>
							<option value="mB" <?php  echo ($inputMannschaft == "mB") ? "selected":"" ?>>m&auml;nnliche B-Jugend</option>
							<option value="wB" <?php  echo ($inputMannschaft == "wB") ? "selected":"" ?>>weibliche B-Jugend</option>
							<option value="mC" <?php  echo ($inputMannschaft == "mC") ? "selected":"" ?>>m&auml;nnliche C-Jugend</option>
							<option value="wC" <?php  echo ($inputMannschaft == "wC") ? "selected":"" ?>>weibliche C-Jugend</option>
							<option value="mD" <?php  echo ($inputMannschaft == "mD") ? "selected":"" ?>>m&auml;nnliche D-Jugend</option>
							<option value="wD" <?php  echo ($inputMannschaft == "wD") ? "selected":"" ?>>weibliche D-Jugend</option>
							<option value="minis" <?php  echo ($inputMannschaft == "minis") ? "selected":"" ?>>Minis</option>
						</select>
					</td>
				</tr>
				<tr>
					<th><label>Ort (Format: PLZ Ortname):</label></th>
					<td>
						<input type="text" name="Ort" value="<?php echo $inputOrt ?>" />
					</td>
				</tr>
			</table>
		</fieldset>
		
		<br />
		<button type="button" onclick="window.location.replace('?site=AntragNeu')">Abbrechen</button>
		<button type="submit">Weiter</button>
	</form>
</p>