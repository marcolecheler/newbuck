<?php
	session_start();
	echo "Bearbeiter: ".$_SESSION['Nachname'].", ".$_SESSION['Vorname'];
	
	//	Klassen einbeziehen
	require_once 'classes/Fahrt.php';
	
	//Testing
	
	if (isset($_SESSION['Fahrten_Array1']))
	{
		echo "FahrtenArray1 existiert<br />".$_SESSION['Fahrten_Array1']->$_Datum."<br /><br />";
	}
?>
<!DOCTYPE html>
<html lang="de">
	<head>
		<meta charset="utf-8">

		<title>Abrechnungssystem TVM Handball</title>
		<meta name="description" content="not-set">
		<meta name="author" content="Marco Lecheler">
		
		<link href="css/loader.css" rel="stylesheet" type="text/css"/>

		<link rel="shortcut icon" href="http://memmingen-handball.de/templates/tvmhandballtemplate1_9/favicon.ico">
		<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
	</head>

	<body>
		<div>
			<header>
				<h1>TVM Handball Abrechnungssystem</h1>
			</header>
			<nav>
				<p><a href="?site=AntragNeu">Antrag einreichen</a></li></p>
				<p><a href="?site=AntragEinsehen">Antrag einsehen</a></p>
			</nav>

			<div class="wrap">
				<?php
					if(isset($_GET['site']))
					{
						switch ($_GET['site']) {
							case 'AntragNeu':
								require_once 'sites/AntragNeu.php';
								break;
								
							case 'AntragEinsehen':
								require_once 'sites/AntragEinsehen.php';
								break;
							
							case 'neueFahrt':
								require_once 'sites/AntragNeueFahrt.php';
								break;
								
							case 'Personendaten':
								require_once 'sites/AntragPersonendaten.php';
								break;
							
							default:
								require_once 'sites/home.php';
								break;
						}
					}
					else
					{
						require_once 'sites/home.php';
					}
									
				?>
			</div>
			<footer><p><a href="admin/">Administration</a></p></footer>
		</div>
	</body>
</html>
