<?php
    include 'GMaps.php';
    
    class Fahrt
    {
    	
		
        public $_Datum;
		public $_Mannschaft;
		public $_Anlass;
		public $_Beleg;
		public $_alternativOrt;
		
		private $_Ort;
		private $_Betrag;
		
		
        function __construct()
        {
            // nothing.
        }
		
		/*
		 * 		Setzt den Ort
		 * 			Pr�ft ob Ort existiert, wenn nicht wird der False zur�ckgegeben
		 */
		public function setOrt($p_Ort)
		{
			$result = GMaps::checkAddress($p_Ort);
			if ($result == null)
			{
				$this->_Ort = null;
				return FALSE;
			}
			else
			{
				//	Ort eintragen
				$this->_Ort = $result;
				return TRUE;
			}
			
		}
		public function getOrt()
		{
			return $this->_Ort;
		}
		
		
		public function getKMeinfach()
		{
			if ($this->_Ort == null)
			{
				//	Kein Ort hinterlegt
				return null;
			}
			else
			{
				//	Distanz von BBZ berechnen
				$from = "Bodenseestr. 41, 87700 Memmingen";
				return GMaps::getDistance($from, $this->_Ort);
			}
		}
		
		
		
		public function ist_vollstaendig()
		{
			
			return false;
		}
    }
    
?>