<?php
	//	Vorbelegte Werte auslesen
	if (isset($_SESSION['Vorname']))
	{
		$inputVorname = $_SESSION['Vorname'];
		$inputNachname = $_SESSION['Nachname'];
		$inputEMail = $_SESSION['EMail'];
		$inputKontoinhaber = $_SESSION['Kontoinhaber'];
		$inputBank = $_SESSION['Bank'];
		$inputIBAN = $_SESSION['IBAN'];
		$inputBIC = $_SESSION['BIC'];
		
	}
	else
	{
		$inputVorname = "TMarco";
		$inputNachname = "TLecheler";
		$inputEMail = "TEmail@marco.lecheler.com";
		$inputKontoinhaber = "TInhaber Lecheler";
		$inputBank = "TBank";
		$inputIBAN = "TIBAN";
		$inputBIC = "TBIC";
	}
	
?>

<h1>Kontaktdaten</h1>
<p>
	<form method="post" action="?site=AntragNeu">
		<fieldset>
			<legend>Kontaktdaten</legend>
			<table>
				<tr>
					<th><label>Vorname:</label></th>
					<td><input name="Vorname" value="<?php echo $inputVorname ?>" required/></td>
				</tr>
				<tr>
					<th><label>Nachname:</label></th>
					<td><input name="Nachname" value="<?php echo $inputNachname ?>" required/></td>
				</tr>
				<tr>
					<th><label>E-Mail:</label></th>
					<td><input name="EMail" value="<?php echo $inputEMail ?>" required/></td>
				</tr>
			</table>
	
		</fieldset>
		
		<fieldset>
			<legend>Kontodaten</legend>
			<table>
				<tr>
					<th><label>Kontoinhaber:</label></th>
					<td><input name="Kontoinhaber" value="<?php echo $inputKontoinhaber ?>" required/></td>
				</tr>
				<tr>
					<th><label>Bank:</label></th>
					<td><input name="Bank" value="<?php echo $inputBank ?>" required/></td>
				</tr>
				<tr>
					<th><label>IBAN:</label></th>
					<td><input name="IBAN" value="<?php echo $inputIBAN ?>" required/></td>
				</tr>
				<tr>
					<th><label>BIC:</label></th>
					<td><input name="BIC" value="<?php echo $inputIBAN ?>" required/></td>
				</tr>
			</table>
	
		</fieldset>
		
		<br />
		<button type="submit">Weiter</button>
	</form>
</p>
