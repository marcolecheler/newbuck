<h1>Fahrt hinzuf&uuml;gen</h1>
<p>
	<form method="post" enctype="multipart/form-data">
		<fieldset>
			<legend>Beleg</legend>
			<p>Falls eine Quittung oder ein anderer Beleg vorliegt, bitte hier angeben und hochladen.</p>
			<table>
				<tr>
					<th><label>Quittung:</label></th>
					<td>
						<input type="file" name="Beleg"/>
						<input type="hidden" name="Belegsenden" value="1" />
					</td>
				</tr>
				<tr>
					<th></th><td style="font-size: x-small">(Erlaubte Dateiendungen: .jpg, .png, .pdf | max 20 MB)</td>
				</tr>
			</table>
		</fieldset>
		
		<br />
		<button type="button" onclick="window.location.replace('?site=AntragNeu')">Abbrechen</button>
		<button type="button" disabled>Allgemein &#10004;</button>
		<button type="button" disabled>Anlass &#10004;</button>
		<button type="submit">Weiter</button>
	</form>
</p>