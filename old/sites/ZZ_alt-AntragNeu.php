<h1>Antrag einrichen</h1>

<form>
	<fieldset>
		<legend>Fahrtdaten</legend>
	
		<p>
			<h2>Datum:</h2>
			<input type="date" name="date" required/>
			<br><br>
		</p>
		<p>
			<h2>Mannschaft:</h2>
			<select>
				<optgroup label="Erwachsenenbereich">
					<option>Herren I</option>
					<option>Herren II</option>
					<option>Damen</option>
				</optgroup>
				<optgroup label="Jugendbereich">
					<option>mA-Jugend</option>
					<option>wA-Jugend</option>
					<option>mB-Jugend</option>
					<option>wB-Jugend</option>
				</optgroup>
			</select>
			<br /><br>
		</p>
		<p>
			<table>
				<tr><th><h2>Fahrtgrund:</h2></th><th><h2>Kostenquelle:</h2></th></tr>
				<tr>
					<td>
						<select>
							<option>Punktspiel</option>
							<option>Qualifikationsspiel</option>
							<option>Vorbereitungsspiel</option>
							<option>Vereinsfahrt</option>
							<option>&Uuml;L-Fahrt</option>
							<option>(sonstige)*</option>
						</select>
					</td>
					<td>
						<select>
							<option>Fahrtkosten</option>
							<option>Meldegeb&uuml;hr</option>
							<option>Schiedsrichtergeb&uuml;hr</option>
							<option>(sonstige)*</option>
						</select>
					</td>
				</tr>
			</table>
			<div style="font-size: xx-small">* Bitte im Kommentar genaueres angeben</div>
			<br /><br>
		</p>
		<p>
			<h2>Ziel-Ort:</h2>
			<input name="Ziel" required/>
			<br><br>
		</p>
		<p>
			<h2>Besteht eine Quittung?</h2>
			on off switch
			<br><br>
		</p>
	</fieldset>
	<br /><br />
	<fieldset>
		<legend>Abrechnungsdaten</legend>
		
	</fieldset>
	<br /><button>Antrag stellen</button>
</form>