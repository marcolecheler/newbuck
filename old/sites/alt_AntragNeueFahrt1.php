<?php
	
	//	Vorbelegte Werte auslesen
	if (isset($_SESSION['Abrechnungsart']))
	{
		$inputAbrechnungsart = $_SESSION['Abrechnungsart'];
	}
	else
	{
		$inputAbrechnungsart = "Punktspiel";
	}
?>

<h1>Fahrt hinzuf&uuml;gen</h1>
<p>
	<form method="post">
		<fieldset>
			<legend>Fahrtgr&uuml;nde</legend>
			<table>
				<tr>
					<th><label>Abrechnungsart:</label></th>
					<td>
						<select name="Abrechnungsart">
							<option <?php  echo ($inputAbrechnungsart == "Punktspiel") ? "selected":"" ?>>Punktspiel</option>
							<option <?php  echo ($inputAbrechnungsart == "Qualifikationsspiel") ? "selected":"" ?>>Qualifikationsspiel</option>
							<option <?php  echo ($inputAbrechnungsart == "Vorbereitungsspiel") ? "selected":"" ?>>Vorbereitungsspiel</option>
							<option <?php  echo ($inputAbrechnungsart == "Vereinsfahrt") ? "selected":"" ?>>Vereinsfahrt</option>
							<option <?php  echo ($inputAbrechnungsart == "UeL-Fahrt") ? "selected":"" ?> value="UeL-Fahrt">&Uuml;L-Fahrt</option>
						</select>
					</td>
				</tr>
			</table>
		</fieldset>
		
		<br />
		<button type="button" onclick="window.location.replace('?site=AntragNeu')">Abbrechen</button><button type="submit">Weiter</button>
	</form>
</p>