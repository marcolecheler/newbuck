<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<body>

<?php
	if(isset($_POST['send']) && $_POST['send'] == "1")
	{
	       
	    //	Pr�fen ob Dateiendung erlaubt ist
	    $filename = $_FILES['Beleg']['name']; 	// orginal File Name
		$fileext = explode(".", $filename); 	// vom orginal File ext. ermitteln
		$fileext_1 = $fileext[1]; 				// ext. neu zuordnen
		$fileext_1 = strtolower($fileext_1); 	// f�r Unix Server
		
		$allowedFileext = ['jpg', 'png', 'pdf'];
		if(!in_array($fileext_1, $allowedFileext))
		{
			echo "Dateiendung wird nicht unterstuetzt!";	
		}
		else
		{
			//	Dateiendung okey
			
			//	Dateigr��e pr�fen (bis 25 MB)
			if(((filesize($_FILES['Beleg']['tmp_name'])/ 1024 / 1024) >= 25))
			{
				echo "Datei zu gro�";
			}
			else
			{
				//	Dateiname erstellen
			    $filename = str_replace(array("�","�","�","�","�","�","�"," "), array("ae","Ae","ue","Ue","oe","Oe","ss","_"), $_FILES['Beleg']['name']);
				$timestamp = time();
				$stamp = date("YmdHis",$timestamp);
				$savename = $stamp .'_'.$filename;
				
				//	Datei verschieben
				$uploaddir = '/var/www/it-switch_de/handball/abrechnungssystem/Belege/';
				
				if(move_uploaded_file($_FILES['Beleg']['tmp_name'], $uploaddir . $savename))
				{
		    		echo "Datei erfolgreich hochgeladen.";
		    	}
		    	else
		    	{
			        echo "Fehler beim Hochladen der Datei.";
			        print_r($_FILES);
		    	}				
			}
		}			
	}
?>

<!-- HTML-Formular: -->

<form enctype="multipart/form-data" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">

<input type="file" name="Beleg" />

<input type="hidden" name="send" value="1" />
<input type="submit" value="Datei hochladen" />
</form>

</body>
</html>