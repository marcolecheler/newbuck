<?php
	session_start();
	
	//require_once 'classes/Kontakt.php';	
	
?>
<!DOCTYPE html>
<html lang="de">
	<head>
		<meta charset="utf-8">

		<title>Abrechnungssystem TVM Handball</title>
		<meta name="description" content="not-set">
		<meta name="author" content="Marco Lecheler">
		
		<link href="css/loader.css" rel="stylesheet" type="text/css"/>

		<link rel="shortcut icon" href="http://memmingen-handball.de/templates/tvmhandballtemplate1_9/favicon.ico">
		<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
	</head>

	<body>
		<div>
			<header>
				<h1>TVM Handball Abrechnungssystem</h1>
			</header>
			<nav>
				<p><a href="?site=AntragNeu">Antrag einreichen</a></li></p>
				<p><a href="?site=zeigeAntrag">Antrag einsehen</a></p>
			</nav>

			<div class="wrap">
				<?php
					
					//	Formulardaten speichern
					if (isset($_POST['Vorname']))
					{
						$_SESSION['Kontaktdaten_Vorname'] = $_POST['Vorname'];
						$_SESSION['Kontaktdaten_Nachname'] = $_POST['Nachname'];
						$_SESSION['Kontaktdaten_EMail'] = $_POST['EMail'];
						$_SESSION['Kontaktdaten_Kontoinhaber'] = $_POST['Kontoinhaber'];
						$_SESSION['Kontaktdaten_Bank'] = $_POST['Bank'];
						$_SESSION['Kontaktdaten_IBAN'] = $_POST['IBAN'];
						$_SESSION['Kontaktdaten_BIC'] = $_POST['BIC'];
						
						//	Aufrufen der n�chsten Seite:
						require_once 'sites/AntragUebersicht.php';
					}
					elseif (isset($_POST['Datum']))
					{
						$_SESSION['Fahrt'.($_SESSION['Fahrten_Anzahl'] + 1).'Datum'] = $_POST['Datum'];
						$_SESSION['Fahrt'.($_SESSION['Fahrten_Anzahl'] + 1).'Mannschaft'] = $_POST['Mannschaft'];
						$_SESSION['Fahrt'.($_SESSION['Fahrten_Anzahl'] + 1).'Ort'] = $_POST['Ort'];
						
						//	Aufrufen der n�chsten Seite:
						require_once 'sites/FahrtNeu2.php';
					}
					elseif (isset($_POST['Anlass']))
					{
						$_SESSION['Fahrt'.($_SESSION['Fahrten_Anzahl'] + 1).'Anlass'] = $_POST['Anlass'];
						
						//	Aufrufen der n�chsten Seite:
						require_once 'sites/FahrtNeu3.php';
					}
					elseif(isset($_POST['Belegsenden']) && $_POST['Belegsenden'] == "1")
					{
						//testing
						echo "<br />Name: ".$_FILES['Beleg']['name']."<br />";
						
						
						//	Dateiupload pr�fen
						$statusOK = true;
						
						if($_FILES['Beleg']['name'] == null)
						{
							//echo "keine Datei angegeben";
						}
						else
						{
							//	Pr�fen ob Dateiendung erlaubt ist
						    $filename = $_FILES['Beleg']['name']; 	// orginal File Name
							$fileext = explode(".", $filename); 	// vom orginal File ext. ermitteln
							$fileext_1 = $fileext[1]; 				// ext. neu zuordnen
							$fileext_1 = strtolower($fileext_1); 	// f�r Unix Server
							
							$allowedFileext = ['jpg', 'png', 'pdf', 'jpeg'];
							if(!in_array($fileext_1, $allowedFileext))
							{
								$statusMessage = "Dateiendung wird nicht unterstuetzt!";
								$statusOK = false;	
							}
							
							
							//	Dateiendung okey
							
							//	Dateigr��e pr�fen (bis 25 MB)
							if($statusOK && ((filesize($_FILES['Beleg']['tmp_name'])/ 1024 / 1024) >= 25))
							{
								$statusMessage = "Datei zu gro�";
								$statusOK = false;	
							}
							
							//	Dateiname erstellen & verschieben
							if($statusOK)
							{
								$filename = str_replace(array("�","�","�","�","�","�","�"," "), array("ae","Ae","ue","Ue","oe","Oe","ss","_"), $_FILES['Beleg']['name']);
								$timestamp = time();
								$stamp = date("YmdHis",$timestamp);
								$savename = $stamp .'_'.$filename;
								
								
								//	Datei verschieben
								$uploaddir = '/var/www/it-switch_de/handball/abrechnungssystem/Belege/';
								
								if(!move_uploaded_file($_FILES['Beleg']['tmp_name'], $uploaddir.$savename))
						    	{
							        $statusMessage = "Fehler beim Hochladen der Datei.";
							        $statusOK = false;
							        //print_r($_FILES);
						    	}
								else
								{
									//	Erfolgreich
									$_SESSION['Fahrt'.($_SESSION['Fahrten_Anzahl'] + 1).'Beleg'] = $savename;	
								}		
							}
						}

						//	Aufrufen der n�chsten Seite:
						require_once 'sites/FahrtNeu4.php';
					}
					elseif (isset($_POST['Fahrthinzufuegen']))
					{
						//	Fahrt hinzuf�gen
						$_SESSION['Fahrten_Anzahl'] = $_SESSION['Fahrten_Anzahl'] + 1;
						
						
						
						//	Aufrufen der n�chsten Seite:
						require_once 'sites/AntragUebersicht.php';
					}
					
					else
					{
						if(isset($_GET['site']))
						{
							switch ($_GET['site']) {
								case 'zeigeAntrag':
									//	Bestehenden Antrag einsehen
									require_once 'sites/zeigeAntrag.php';
									break;
									
								case 'AntragNeu':
									
									//	Pr�fen ob Personaldaten bereits eingetragen
									if (isset($_SESSION['Kontaktdaten_Vorname']))
									{
										require_once 'sites/AntragUebersicht.php';
									}
									else
									{
										require_once 'sites/Personendaten.php';
									}
									
									
									break;
								
								case 'Personendaten':
									require_once 'sites/Personendaten.php';
									break; 	
								
								case 'FahrtNeu1':
									
									require_once 'sites/FahrtNeu1.php';
									break;
									
								case 'FahrtEntfernen':
									
									if (isset($_GET['FahrtID']))
									{
										//	Einzelne Fahrt entfernen
										
										//	Beleg l�schen
										if (!($_SESSION['Fahrt'.$_GET['FahrtID'].'Beleg'] == null))
										{
											unlink('Belege/'.$_SESSION['Fahrt'.$_GET['FahrtID'].'Beleg']);
										}
										
										
										if ($_GET['FahrtID'] == 1)
										{
											//	Werte leeren
											$_SESSION['Fahrt1Datum'] = null;
											$_SESSION['Fahrt1Mannschaft'] = null;
											$_SESSION['Fahrt1Anlass'] = null;
											$_SESSION['Fahrt1Ort'] = null;
											$_SESSION['Fahrt1Beleg'] = null;
										}
										else
										{
											for ($i=$_GET['FahrtID']; $i <= ($_SESSION['Fahrten_Anzahl'] - 1); $i++)
											{
												//	Werte vom n�chsten holen
												$_SESSION['Fahrt'.$i.'Datum'] = $_SESSION['Fahrt'.($i + 1).'Datum'];
												$_SESSION['Fahrt'.$i.'Mannschaft'] = $_SESSION['Fahrt'.($i + 1).'Mannschaft'];
												$_SESSION['Fahrt'.$i.'Anlass'] = $_SESSION['Fahrt'.($i + 1).'Anlass'];
												$_SESSION['Fahrt'.$i.'Ort'] = $_SESSION['Fahrt'.($i + 1).'Ort'];
												$_SESSION['Fahrt'.$i.'Beleg'] = $_SESSION['Fahrt'.($i + 1).'Beleg'];
											}
										}
										
										
										
										//	Fahrtenz�hler um 1 runter setzen
										$_SESSION['Fahrten_Anzahl'] = $_SESSION['Fahrten_Anzahl'] - 1;
									}
									
									
									require_once 'sites/AntragUebersicht.php';
									break;
									
								case 'AntragSenden':
									require_once 'sites/AntragSenden.php';
									break;
									
									
								default:
									
									require_once 'sites/home.php';
									break;
							}
						}
						else
						{
							require_once 'sites/home.php';
						}	
					}
					
					//	N�chste Seite aufrufen
					
				
				
				?>
			</div>
			<footer><p><a href="admin/">Administration</a></p></footer>
		</div>
	</body>
</html>
