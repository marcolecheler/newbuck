<?php
        
    /**
     * 		Google Berechnungen und pr�fungen
     */
     
     
    class GMaps
    {
        
        static public function getDistance($p_from, $p_to)
        {		
			//	Umlaute ersetzen
			$umlaute = Array("�" => "ae", "�" => "ue", "�" => "oe", "�" => "Ae", "�" => "Ue", "�" => "Oe"); 
			$from = strtr($p_from, $umlaute);
			$to = strtr($p_to, $umlaute);
			
			//	URL-Dastellung
			$from = urlencode($from);
			$to = urlencode($to);
			
			//	Daten abgreifen
			$data = file_get_contents("http://maps.googleapis.com/maps/api/distancematrix/json?origins=$from&destinations=$to&language=en-EN&sensor=false");
			//	Datenformat anpassen
			$data = mb_convert_encoding($data, 'HTML-ENTITIES', "UTF-8");
			$data = json_decode($data);
			
			//	Werte zuruecksetzen
			$distance = 0;
			
			//	Daten auslesen
			foreach($data->rows[0]->elements as $road)
			{
			    $distance += $road->distance->value;
			}
            
			//	Daten ausgeben in Meter
			return $distance;
        }
		
		
		/**
		 * 		Pr�fen ob Adresse existiert
		 */
		static public function checkAddress($p_address)
		{	
			//	Umlaute ersetzen
			$umlaute = Array("�" => "ae", "�" => "ue", "�" => "oe", "�" => "Ae", "�" => "Ue", "�" => "Oe"); 
			$address = strtr($p_address, $umlaute);
			
			//	URL-Dastellung
			$address = urlencode($address);
			
			//	Daten abgreifen
			$data = file_get_contents("http://maps.googleapis.com/maps/api/distancematrix/json?origins=$address&destinations=$address&language=en-EN&sensor=false");
			//	Datenformat anpassen
			$data = mb_convert_encoding($data, 'HTML-ENTITIES', "UTF-8");
			$data = json_decode($data);
			
			
			
			
			if($data->destination_addresses[0] == " ")
			{
				return null;
			}
			else
			{
				return $data->destination_addresses[0];
			}
		}
		
		
    }
    
?>